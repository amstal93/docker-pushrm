FROM chko/docker-pushrm:1
COPY entrypoint.sh Dockerfile README.md /
ENTRYPOINT [ "/entrypoint.sh" ]
